package com.example.tp2_utilisateur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tp2UtilisateurApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tp2UtilisateurApplication.class, args);
	}

}
