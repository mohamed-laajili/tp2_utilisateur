package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Utilisateur;
import com.example.demo.repositories.UtilisateurRepo;

@Service
public class UtilisateurService {
    @Autowired
    private UtilisateurRepo utilisateurRepository;

    public void creerUtilisateur(Utilisateur utilisateur) {
    	utilisateurRepository.save(utilisateur);
    }

    public Utilisateur obtenirUtilisateur(Long id) {
        return utilisateurRepository.findById(id).get();
    }

    public void mettreAJourUtilisateur(Long id, Utilisateur utilisateur) {
        Utilisateur existingUtilisateur = utilisateurRepository.findById(id).orElse(null);
        if (existingUtilisateur != null) {
            // Mettez à jour les champs de l'utilisateur existant avec les valeurs de l'utilisateur fourni
            existingUtilisateur.setNom(utilisateur.getNom());
            existingUtilisateur.setPrénom(utilisateur.getPrénom());
            existingUtilisateur.setLogin(utilisateur.getLogin());
            existingUtilisateur.setMot_de_passe(utilisateur.getMot_de_passe());
            existingUtilisateur.setEmail(utilisateur.getEmail());
            // Vous pouvez mettre à jour d'autres champs ici
            utilisateurRepository.save(existingUtilisateur);
        }
    }

    public void supprimerUtilisateur(Long id) {
        utilisateurRepository.deleteById(id);
    }
}

